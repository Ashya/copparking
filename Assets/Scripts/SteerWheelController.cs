﻿using UnityEngine;
using System.Collections;

public class SteerWheelController : MonoBehaviour {

    public float  maximumAngle = 180;

    public void Move(float moveAmount) {
        float angle = -moveAmount * maximumAngle;
        transform.rotation = Quaternion.AngleAxis( angle, Vector3.forward );
    }

    void OnDrag() {

    }
}
