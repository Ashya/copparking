﻿using UnityEngine;
using System.Collections;

public class ParkingAreaController : SingletonBehaviour<ParkingAreaController> {

    [Separator("General Settings")]
    public GameObject parkingPlane;
    private Vector3 size;

    [Separator("Area Booleans")]
    public bool canBeParked = false;
    private bool created = false;

	void Awake () {
        if( !created ) {
            GameManager.OnGameStart += this.OnGameStart;
            GameManager.OnGameEnd += this.OnGameEnd;
            size = this.gameObject.transform.localScale;
        } else {
            DestroyImmediate( instance );
        }
    }

    void OnGameStart() {
        parkingPlane = GameObject.Find( "ParkingPlane" );

        parkingPlane.transform.localRotation = Quaternion.identity;
        parkingPlane.transform.localPosition = Vector3.zero;
        parkingPlane.transform.localScale = new Vector3( 1/ (transform.localScale.x * 2.8f), 1, 1/ (transform.localScale.z * 1.75f));

        canBeParked = true;
    }

    void OnGameEnd(bool win) {
        canBeParked = false;
    }
}
