﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingPanelController : MonoBehaviour {

    [Separator("Game objects")]
    public Image loadingImage;
    public Text loadingText;

    [Separator("Animation")]
    [Range(0,100)]
    public float speed = 50f;
    public bool rotateRight = false;

    [Separator("Tips And Tricks")]
    public string[] tipsAndTricks;
    private bool shown = false;

	void Update () {
        if(GameManager.instance.loading) {
            if( tipsAndTricks.Length > 0 && !shown) {
                shown = true;

                int rand = Random.Range( 0, tipsAndTricks.Length - 1 ); 
                loadingText.text = "[TIP]  " + tipsAndTricks[rand];
            }

            if(!rotateRight)
                loadingImage.transform.Rotate( Vector3.back, -Time.deltaTime * speed );
            else
                loadingImage.transform.Rotate( Vector3.back, Time.deltaTime * speed );
        } else {
            shown = false;
        }	
	}
}
