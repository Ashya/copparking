﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class CheckpointHelper : MonoBehaviour {

    public bool showHelpersWhilePlaying = false;

    [Range(2,10)]
    public float checkpointSize = 5f;

#if UNITY_EDITOR
    void OnDrawGizmos() {
        if(!Application.isPlaying || showHelpersWhilePlaying) {
            Transform spawnPoint = transform.Find( "SpawnPoint" );
            Transform parkingArea = transform.Find( "ParkingArea" );
            Transform checkpointsContainer = transform.Find( "Checkpoints" );

            List<Transform> checkpoints = new List<Transform>();

            if( checkpointsContainer ) {
                foreach( Transform checkpoint in transform.Find( "Checkpoints" ) )
                    checkpoints.Add( checkpoint );

                if( checkpoints.Count > 0 ) {
                    checkpoints.Sort( ( x, y ) => string.Compare( x.name, y.name ) );

                    for( int i = 0 ;i < checkpoints.Count ;i++ ) {
                        Gizmos.DrawWireSphere( new Vector3( checkpoints[i].transform.position.x, checkpoints[i].transform.position.y, checkpoints[i].transform.position.z ), checkpointSize/3 );
                        //Gizmos.DrawWireCube( new Vector3( checkpoints[i].transform.position.x, checkpoints[i].transform.position.y, checkpoints[i].transform.position.z ), new Vector3( checkpointSize, 1, checkpointSize ) );
                    }

                    for( int i = 0 ;i < checkpoints.Count - 1 ;i++ ) {
                        Gizmos.DrawLine( checkpoints[i].transform.position, checkpoints[i + 1].transform.position );
                    }

                    Gizmos.DrawLine( checkpoints[0].transform.position, spawnPoint.position );
                    Gizmos.DrawLine( checkpoints[checkpoints.Count - 1].transform.position, parkingArea.position );
                }
            }
        }
    }
#endif
}
