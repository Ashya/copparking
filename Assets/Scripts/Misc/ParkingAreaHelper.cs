﻿using UnityEngine;
using System.Collections;

public class ParkingAreaHelper : MonoBehaviour {

    public Vector3 parkingAreaSize = new Vector3();

#if UNITY_EDITOR
    void OnDrawGizmos () {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube( new Vector3(transform.position.x, transform.position.y + parkingAreaSize.y/ 2, transform.position.z), parkingAreaSize);
	}
#endif

}
