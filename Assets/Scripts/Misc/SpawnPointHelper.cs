﻿using UnityEngine;
using System.Collections;

public class SpawnPointHelper : MonoBehaviour
{

#if UNITY_EDITOR
    void OnDrawGizmos() {
        Gizmos.color = Color.green;
        Gizmos.DrawCube( transform.position, new Vector3(5,5,5) );
        Gizmos.color = Color.blue;
        Gizmos.DrawRay( transform.position, transform.forward * 5 );
        Gizmos.color = Color.red;
        Gizmos.DrawRay( transform.position, transform.right * 5 );
        Gizmos.color = Color.green;
        Gizmos.DrawRay( transform.position, transform.up * 5 );
    }
#endif

}
