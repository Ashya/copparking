﻿using System.Collections;

public class TagManager {
    public static readonly string spawnPoint = "SpawnPoint";
    public static readonly string parkingArea = "ParkingArea";
    public static readonly string checkpoint = "checkpoint";
    static readonly string obstacleLayerName = "Obstacles";
    public static int obstacleLayer { get { return UnityEngine.LayerMask.NameToLayer(TagManager.obstacleLayerName); } }
}
