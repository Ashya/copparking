﻿using UnityEngine;
using System.Collections;

public class VehicleBase : MonoBehaviour {

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- VARIABLES
    //--------------------------------------------------------------------------------------
    public float speed = 0;

    [HideInInspector]
    public bool dead = false;

    [Header("VEHICLE SOUNDS")]
    public AudioSource engineSound;
    public AudioSource crashSound;

    [Header("SETTINGS")]
    public float minParkingSpeed = 1f;

    protected bool respawnAfterContinue = false;

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------- SOUNDS
    //--------------------------------------------------------------------------------------
    public void SoundOn() {
        if(engineSound != null) {
            engineSound.mute = GameManager.instance.settings.muteFX;
            engineSound.Play();
        }else {
            Debug.LogWarning( this.name + ": Engine sound NOT set!");
        }
    }

    public void SoundOff () {
        if(engineSound != null) {
            engineSound.mute = true;
        } else {
            Debug.LogWarning( this.name + ": Crash sound NOT set!" );
        }
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------- STOP VEHICLE
    //--------------------------------------------------------------------------------------
    public void Stop () {
        Rigidbody rigidbody = GetComponent<Rigidbody>();

        if(!rigidbody.isKinematic) {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
        }
	}

    virtual public bool MostlyStopped() {
        return false;
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------- RESPAWNING
    //--------------------------------------------------------------------------------------
    public void RespawnAfterContinue() {
        if(respawnAfterContinue) {
            Stop();
            GetComponent<Rigidbody>().MovePosition( VehicleManager.instance.spawnPoint.position );
            transform.position = VehicleManager.instance.spawnPoint.position;
            transform.rotation = VehicleManager.instance.spawnPoint.rotation;
        }
    }

    virtual public void ResumeFromContinue() {
    }
}
