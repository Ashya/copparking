﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor( typeof( ShopManager ) )]
class ShopManagerEditor : Editor
{
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        ShopManager shop = (ShopManager)target;

        EditorGUILayout.Space();

        EditorGUILayout.HelpBox( "PLAYERS WALLET", MessageType.Info );
        shop._cash = EditorGUILayout.IntField( "Cash: ", shop._cash );

        EditorGUILayout.HelpBox( "SHOP PRICES", MessageType.Info );
        
        shop.levelPrice = EditorGUILayout.IntSlider( "Level: ", shop.levelPrice, 0, 1000 );
        shop.vehiclePrice = EditorGUILayout.IntSlider( "Vehicle: ", shop.vehiclePrice, 0, 10000 );

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.HelpBox( "CHEATS BUTTONS", MessageType.Warning );
        if( GUILayout.Button( "Add " + ShopManager.instance.levelPrice + " cash" ) ) {
            shop.AddCashForLevel();
        }
        if( GUILayout.Button( "Add " + ShopManager.instance.vehiclePrice + " cash" ) ) {
            shop.AddCashForVehicle();
        }
        if( GUILayout.Button( "Remove 100 cash" ) ) {
            shop.Remove100Cash();
        }

        EditorGUILayout.Space();
    }
}