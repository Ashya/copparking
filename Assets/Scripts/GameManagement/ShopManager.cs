﻿using UnityEngine;
using System.Collections;

public class ShopManager : SingletonBehaviour<ShopManager> {

    //----------------------------------------------------------------
    //----------------------------------------------------------- VARS
    //----------------------------------------------------------------

    private bool created = false;

    [HideInInspector]
    public int _cash;
    [HideInInspector]
    public int levelPrice = 100;
    [HideInInspector]
    public int vehiclePrice = 1000;

    //----------------------------------------------------------------
    //--------------------------------------------------------- CHEATS
    //----------------------------------------------------------------

    [ContextMenu("Add cash for level")]
    public void AddCashForLevel() {
        AddCash( levelPrice );
    }

    [ContextMenu( "Add cash for vehicle" )]
    public void AddCashForVehicle() {
        AddCash( vehiclePrice );
    }

    [ContextMenu( "Remove 100 cash" )]
    public void Remove100Cash() {
        BuyItem( 100 );
    }

    //----------------------------------------------------------------
    //----------------------------------------------------------- INIT
    //----------------------------------------------------------------

    void Init() {
        _cash = PlayerPrefs.GetInt("PlayerCash");
    }

    //----------------------------------------------------------------
    //---------------------------------------------------------- AWAKE
    //----------------------------------------------------------------

    void Awake () {
        if(!created) {
            DontDestroyOnLoad( instance );
            _instance = this;
            created = true;

            Init();
        } else {
            DestroyImmediate( gameObject );
        }
	}

    //----------------------------------------------------------------
    //----------------------------------------------------- CHECK CASH
    //----------------------------------------------------------------

    public bool HaveEnoughCash(int price ) {
        return price <= _cash;
    }

    //----------------------------------------------------------------
    //------------------------------------------------------- ADD CASH
    //----------------------------------------------------------------

    public void AddCash(int cash ) {
        _cash += cash;
        PlayerPrefs.SetInt( "PlayerCash", _cash);
    }

    //----------------------------------------------------------------
    //------------------------------------------------------- BUY ITEM
    //----------------------------------------------------------------

    public bool BuyItem( int price ) {
        if(HaveEnoughCash(price)) {
            _cash -= price;
            PlayerPrefs.SetInt( "PlayerCash", _cash );
            print( "[" + this.name + "] Item bought!" );

            return true;
        } else {
            print("["+ this.GetComponent<ShopManager>().name +"] Not enought cash!");
            return false;
        }
    }

    //----------------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------ REAL SHOP
    //----------------------------------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------
    //---------------------------------------------- RESTORE PURCHASES
    //----------------------------------------------------------------

    public void RestorePurchases() {
        if( Application.isEditor ) {
            Debug.Log( "[SHOP CONTROLLER] Purchases restore!" );
        }
    }

    //----------------------------------------------------------------
    //------------------------------------------------------- BUY ITEM
    //----------------------------------------------------------------

    public void UnlockAllLevels() {
        GameManager.instance.UnlockAllLevels();
        if( Application.isEditor ) {
            Debug.Log( "[SHOP CONTROLLER] All levels unlocked!" );
        }
    }

    //----------------------------------------------------------------
    //----------------------------------------------------- ADD CASH 1
    //----------------------------------------------------------------

    public void AddSomeGold() {
        AddCash( 350 );
        if( Application.isEditor ) {
            Debug.Log( "[SHOP CONTROLLER] 350 $ added!" );
        }
    }

    //----------------------------------------------------------------
    //----------------------------------------------------- ADD CASH 2
    //----------------------------------------------------------------

    public void AddMoreGold() {
        AddCash( 2250 );
        if( Application.isEditor ) {
            Debug.Log( "[SHOP CONTROLLER] 2250 $ added!" );
        }
    }
}
