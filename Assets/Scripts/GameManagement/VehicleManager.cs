﻿using UnityEngine;
using System.Collections;

//---------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------- [CLASS] VEHICLE MANAGER
//---------------------------------------------------------------------------------------------------------------------------------------------
public class VehicleManager : SingletonBehaviour<VehicleManager> {

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- VARIABLES
    //--------------------------------------------------------------------------------------
    [Separator("Vehicle settings")]
    public GameObject[] vehiclePrefabs;     //Prefabs
    [Space(10)]
    public GameObject currentVehicle;    //Current vehicle

    [Separator("Other settings")]
    public Transform spawnPoint;            //Where to spawn our vehicle
    public CarCamera playerCamera;          //Camera for our vehicle

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------- AWAKE
    //--------------------------------------------------------------------------------------
    void Awake() {
        playerCamera = Camera.main.GetComponent<CarCamera>();
        GameManager.OnGameEnd += OnGameEnd;
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------- START GAME
    //--------------------------------------------------------------------------------------
    public void GameStart() {
        spawnPoint = GameObject.FindGameObjectWithTag( TagManager.spawnPoint ).transform;

        if( currentVehicle != null ) {
            DestroyImmediate( currentVehicle.gameObject );
        }

        GameObject vehicle = Instantiate( vehiclePrefabs[GameManager.instance.selectedCar], spawnPoint.position, spawnPoint.rotation ) as GameObject;
        currentVehicle = vehicle;

        /*if( currentVehicle == null ) {
            Debug.LogError(this.name + ": Missing CurrentVehicle!");
            return;
        }

        playerCamera.target = currentVehicle.transform;
        playerCamera.vehicleFront = currentVehicle.transform.Find( "Front" );

        CarCameras carCamera = currentVehicle.GetComponent<CarCameras>();
        if(carCamera != null) {
            playerCamera.distance = carCamera.viewDistance;
            playerCamera.height = carCamera.viewHeight;
            playerCamera.targetHeightRatio = carCamera.targetHeightRatio;
            playerCamera.topCameraOffset = carCamera.topCameraOffset;
        } else {
            Debug.LogWarning("Missing car camera script, leaving DEFAULT setup!");
            playerCamera.ResetToDefaults();
        }*/
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------- ON GAME END
    //--------------------------------------------------------------------------------------
    void OnGameEnd(bool win) {
        Destroy( currentVehicle.gameObject );
        currentVehicle = null;
    }
}
