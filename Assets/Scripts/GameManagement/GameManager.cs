﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

//---------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------- [CLASS] GAME SETTINGS
//---------------------------------------------------------------------------------------------------------------------------------------------

[Serializable]
public class GameSettings
{
    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- VARIABLES
    //--------------------------------------------------------------------------------------
    bool _muteFX = false;               //mute effects in the game
    bool _muteMusic = false;            //mute music in the game

    float _audioVolume = 1f;            //menu audio volume
    float _inGameAudioVolume = 0.5f;     //inGame audio volume

    //bool _rightHanded = false;          //right or left hand controlls
    bool _crashProtectionFirst = false; //allows to continue game after crashing for the first time

    int _sensitivity = 3;               //controlls sensitivity

    bool _buttonSteering = false;
    bool _wheelSteering = false;
    bool _tiltSteering = false;

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- GET / SET
    //--------------------------------------------------------------------------------------
    public bool muteFX {
        get { return _muteFX; }
        set {
            _muteFX = value;
            PlayerPrefsX.SetBool( "MuteFX", _muteFX );
        }
    }

    public bool muteMusic
    {
        get { return _muteMusic; }
        set
        {
            _muteMusic = value;
            PlayerPrefsX.SetBool( "MuteMusic", _muteMusic );
        }
    }

    public float audioVolume
    {
        get { return _audioVolume; }
        set
        {
            _audioVolume = value;
            PlayerPrefs.SetFloat( "AudioVolume", _audioVolume );
        }
    }

    public float inGameAudioVolume
    {
        get { return _inGameAudioVolume; }
        set
        {
            _inGameAudioVolume = value;
            PlayerPrefs.SetFloat( "InGameVolume", _inGameAudioVolume );
        }
    }

    /*public bool rightHanded
    {
        get { return _rightHanded; }
        set
        {
            _rightHanded = value;
            PlayerPrefsX.SetBool( "RightHanded", _rightHanded );
        }
    }*/

    public bool crashProtectionFirst
    {
        get { return _crashProtectionFirst; }
        set
        {
            _crashProtectionFirst = value;
            PlayerPrefsX.SetBool( "CrashProtectionFirst", _crashProtectionFirst );
        }
    }

    public int sensitivity
    {
        get { return _sensitivity; }
        set
        {
            _sensitivity = value;
            PlayerPrefs.SetInt( "Sensitivity", _sensitivity );
        }
    }

    public bool buttonSteering {
        get { return _buttonSteering; }
        set {
            _buttonSteering = value;
            PlayerPrefsX.SetBool( "ButtonsSteering", _buttonSteering );
        }
    }

    public bool wheelSteering {
        get { return _wheelSteering; }
        set {
            _wheelSteering = value;
            PlayerPrefsX.SetBool( "WheelSteering", _wheelSteering );
        }
    }

    public bool tiltSteering {
        get { return _tiltSteering; }
        set {
            _tiltSteering = value; PlayerPrefsX.SetBool( "TiltSteering", _tiltSteering );
        }
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- INITIALIZATION
    //--------------------------------------------------------------------------------------
    public void Init() {
        _muteFX = PlayerPrefsX.GetBool( "MuteFX", false );
        _muteMusic = PlayerPrefsX.GetBool( "MuteMusic", false );
        _audioVolume = PlayerPrefs.GetFloat( "AudioVolume", 1f );
        _inGameAudioVolume = PlayerPrefs.GetFloat( "InGameVolume", 0.5f );
        //_rightHanded = PlayerPrefsX.GetBool( "RightHanded", true );
        //_crashProtectionFirst = PlayerPrefsX.GetBool( "CrashProtection", false );
        _sensitivity = PlayerPrefs.GetInt( "Sensitivity", 5 );
        _tiltSteering = PlayerPrefsX.GetBool( "TiltSteering", false );
        _wheelSteering = PlayerPrefsX.GetBool( "WheelSteering", true );
        _buttonSteering = PlayerPrefsX.GetBool( "ButtonsSteering", false );
    }

}

//---------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------ [CLASS] GAME MANAGER
//---------------------------------------------------------------------------------------------------------------------------------------------

public class GameManager : SingletonBehaviour<GameManager> {

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- VARIABLES
    //--------------------------------------------------------------------------------------
    private static bool objCreated = false;

    public static Action OnGameStart = () => {};
    public static Action<bool> OnGameEnd = (bool win) => {};
    public static Action OnPause = () => {};
    public static Action OnResume = () => {};
    public static Action OnShowContinue = () => {};
    public static Action OnResumeFromContinue = () => {};

    //from the left: easy, normal, medium, hard
    public enum GameModes { Cadet, Corporal, Sergeant, Captain }
    [Separator("Main settings")]
    public GameModes selectedMode = GameModes.Cadet;

    [Space(10)]
    public int selectedCar = 0;
    public int currentLevel = 0;
    //public int currentChunk = 0;

    [Space(10)]
    public int levelRewardCash = 50;

    int _levels = 18;
    public int levels { get { return _levels; } }

    [Separator("Game booleans")]
    [HideInInspector]
    public bool win = false;
    [Space(10)]
    public bool inGame = false;
    public bool gameEnded = false;
    public bool gamePaused = false;
    public bool gameFailed = false;

    [Space(10)]
    public bool runFromLevel = false;
    public bool loading = false;
    public bool crashed = false;
    private bool continueShown = false;

    [HideInInspector]
    public int[][] levelsUnlocked;
    [HideInInspector]
    public int[][] levelScores;
    [HideInInspector]
    public int[][] levelStars;
    [HideInInspector]
    public float[][] levelTimes;
    [HideInInspector]
    public int[] carsUnlocked;

    [HideInInspector]
    public bool[] modesUnlocked;

    [Separator("Levels GameOver settings")]
    [Space(10)]
    public float[] targetTimes;
    [Space(10)]
    [Range(0,10)]
    public float gameOverDelay = 1.5f;

    [HideInInspector]
    public bool everythingSet = false;

    [Separator("Object containers")]
    [Space(10)]
    public Transform levelsContainer;
    public Transform chunksContainer;
    
    public int gameOverCount = 0;

    [HideInInspector]
    public List<GameObject> gameLevels = new List<GameObject>();
    public GameObject currentChunk;
    //public List<GameObject> mapChunks = new List<GameObject>();

    //[HideInInspector]
    //public GameOverCam gameOverCamera; //camera displaying info when game ends

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------ SOME SETTINGS
    //--------------------------------------------------------------------------------------

    [HideInInspector]
    public GameSettings settings;

    [Separator("Music settings")]
    [Space(10)]
    public AudioClip winSound;
    public AudioClip crashSound;
    public AudioClip musicSound;
    public AudioClip inGameSound;

    [HideInInspector]
    public AudioSource musicSource;
    [HideInInspector]
    public AudioSource winSoundSource;
    [HideInInspector]
    public AudioSource inGameSoundSource;
    [HideInInspector]
    public AudioSource crashSoundSource;

    CheckpointController checkpointController;

    [Space(10)]
    [Range(0,1)]
    public float musicVolumeMenu = 1;
    [Range(0,1)]
    public float musicVolumeGame = 0.3f;

    [HideInInspector]
    public int crashProtectionsFromMode = 0;

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------ THINGS TO BUY
    //--------------------------------------------------------------------------------------

    bool _removeAds = false;
    public bool removeAds { get { return _removeAds; } set { _removeAds = value; PlayerPrefsX.SetBool( "RemoveAds", _removeAds ); } }

    bool _crashProtectionBought = false;
    public bool crashProtectionBought { get { return _crashProtectionBought; } set { _crashProtectionBought = value; PlayerPrefsX.SetBool( "CrashProtectionBought", _crashProtectionBought ); } }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------- CHEATS
    //--------------------------------------------------------------------------------------

    [ContextMenu( "Clear Prefs" )]
    void ClearPlayerPrefs() {
        PlayerPrefs.DeleteAll();
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------- AWAKE
    //--------------------------------------------------------------------------------------

    void Awake() {
        if(!objCreated) {
            DontDestroyOnLoad( instance );
            objCreated = true;
            _instance = this;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            settings = new GameSettings();
            settings.Init();

            SettingsController.instance.InitController();

            levelsContainer = GameObject.Find( "[ + ] LEVELS [ + ]" ).GetComponent<Transform>();
            //chunksContainer = GameObject.Find( "Environment" ).GetComponent<Transform>();

            _removeAds = PlayerPrefsX.GetBool( "RemoveAds", false );
            _crashProtectionBought = PlayerPrefsX.GetBool( "CrashProtectionBought", false );

            modesUnlocked = PlayerPrefsX.GetBoolArray( "ModesUnlocked", false, 3 );

            levelsUnlocked = new int[3][];
            levelScores = new int[3][];
            levelTimes = new float[3][];
            levelStars = new int[3][];

            carsUnlocked = new int[3];

            targetTimes = new float[_levels];

            for(int i=0 ; i<targetTimes.Length ; i++ ) {
                targetTimes[i] = 10;
            }

            levelsUnlocked[0] = PlayerPrefsX.GetIntArray( "LevelsUnlockedCar0", 0, levels );
            levelsUnlocked[1] = PlayerPrefsX.GetIntArray( "LevelsUnlockedCar1", 0, levels );
            levelsUnlocked[2] = PlayerPrefsX.GetIntArray( "LevelsUnlockedCar2", 0, levels );

            levelsUnlocked[0][0] = 1;
            levelsUnlocked[1][0] = 1;
            levelsUnlocked[2][0] = 1;

            levelScores[0] = PlayerPrefsX.GetIntArray( "LevelScoresCar0", 0, levels );
            levelScores[1] = PlayerPrefsX.GetIntArray( "LevelScoresCar1", 0, levels );
            levelScores[2] = PlayerPrefsX.GetIntArray( "LevelScoresCar2", 0, levels );

            levelStars[0] = PlayerPrefsX.GetIntArray( "LevelStarsCar0", 0, levels );
            levelStars[1] = PlayerPrefsX.GetIntArray( "LevelStarsCar1", 0, levels );
            levelStars[2] = PlayerPrefsX.GetIntArray( "LevelStarsCar2", 0, levels );

            levelTimes[0] = PlayerPrefsX.GetFloatArray( "LevelTimesCar0", 0, levels );
            levelTimes[1] = PlayerPrefsX.GetFloatArray( "LevelTimesCar1", 0, levels );
            levelTimes[2] = PlayerPrefsX.GetFloatArray( "LevelTimesCar2", 0, levels );

            carsUnlocked[0] = PlayerPrefs.GetInt( "CarUnlocked0" );
            carsUnlocked[1] = PlayerPrefs.GetInt( "CarUnlocked1" );
            carsUnlocked[2] = PlayerPrefs.GetInt( "CarUnlocked2" );

            carsUnlocked[0] = 1;

            inGame = false;

            foreach(Transform level in levelsContainer ) {
                gameLevels.Add( level.gameObject );
                level.gameObject.SetActive( false );
            }
            gameLevels.Sort( ( x, y ) => string.Compare( x.name, y.name ) );

            /*foreach( Transform chunk in chunksContainer ) {
                mapChunks.Add( chunk.gameObject );
                chunk.gameObject.SetActive( false );
            }
            mapChunks.Sort( ( x, y ) => string.Compare( x.name, y.name ) );*/

            checkpointController = GetComponent<CheckpointController>();

            everythingSet = true;
        } else {
            DestroyImmediate( gameObject );
        }
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------- START
    //--------------------------------------------------------------------------------------

    void Start() {
        if( musicSound != null ) {
            musicSource = gameObject.AddComponent<AudioSource>();
            musicSource.clip = musicSound;
            musicSource.mute = settings.muteMusic;
            musicSource.loop = true;
            musicSource.volume = settings.audioVolume;
            musicSource.Play();
        }

        if(winSound != null) {
            winSoundSource = gameObject.AddComponent<AudioSource>();
            winSoundSource.playOnAwake = false;
            winSoundSource.clip = winSound;
            winSoundSource.mute = settings.muteFX;
        }

        if( crashSound != null ) {
            crashSoundSource = gameObject.AddComponent<AudioSource>();
            crashSoundSource.playOnAwake = false;
            crashSoundSource.clip = crashSound;
            crashSoundSource.mute = settings.muteFX;
        }

        if(inGameSound != null) {
            inGameSoundSource = gameObject.AddComponent<AudioSource>();
            inGameSoundSource.loop = true;
            inGameSoundSource.playOnAwake = false;
            inGameSoundSource.clip = inGameSound;
            inGameSoundSource.mute = settings.muteMusic;
            inGameSoundSource.volume = settings.inGameAudioVolume;
        }

        if( runFromLevel )
            StartGame();
	}

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- MUSIC CONTROLL
    //--------------------------------------------------------------------------------------
    void MusicOn() {
        if(musicSource != null) {
            musicSource.mute = settings.muteMusic;
        }

        if( inGameSound != null ) {
            inGameSoundSource.mute = settings.muteMusic;
        }
    }

    void MusciOff() {
        if(musicSource != null) {
            musicSource.mute = true;
        }


        if( inGameSound != null ) {
            inGameSoundSource.mute = true;
        }
    }

    public void MusicToggle() {
        if(musicSource != null) {
            musicSource.mute = settings.muteMusic;
        }

        if( inGameSound != null ) {
            inGameSoundSource.mute = settings.muteMusic;
        }
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- MODES CONTROLL
    //--------------------------------------------------------------------------------------
    public void UnlockMode(GameModes mode) {
        switch( mode ) {
            case GameModes.Captain: modesUnlocked[0] = true; break;
            case GameModes.Corporal: modesUnlocked[1] = true; break;
            case GameModes.Sergeant: modesUnlocked[2] = true; break;
        }

        PlayerPrefsX.SetBoolArray( "ModesUnlocked", modesUnlocked );
    }

    public bool IsModeUnlocked(GameModes mode) {
#if UNITY_EDITOR
        return true;
#endif
        switch(mode) {
            case GameModes.Cadet: return true;
            case GameModes.Captain: return modesUnlocked[0] ? true : false;
            case GameModes.Corporal: return modesUnlocked[1] ? true : false;
            case GameModes.Sergeant: return modesUnlocked[2] ? true : false;
            default: return false;
        }
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------ GAME CONTROLL
    //--------------------------------------------------------------------------------------
    public void Pause() {
        gamePaused = true;
        VehicleManager.instance.currentVehicle.GetComponent<CarAudio>().SoundOff();
        Time.timeScale = 0.0f;

        inGameSoundSource.volume = settings.inGameAudioVolume/2;

        OnPause();
    }

    public void Resume() {
        gamePaused = false;
        VehicleManager.instance.currentVehicle.GetComponent<CarAudio>().SoundOn();
        Time.timeScale = 1.0f;

        inGameSoundSource.volume = settings.inGameAudioVolume;

        OnResume();
    }

    public void ShowContinue(bool instantly = false) {
        if(!continueShown) {
            continueShown = true;

            if(instantly) {
                OnShowContinue();
            }
        }
    }

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- GAME OVER
    //--------------------------------------------------------------------------------------
    public void GameOver(bool win) {
        if(!gameEnded) {
            gameEnded = true;
            gamePaused = false;

            if(win)
                StartCoroutine( GameOver( win, gameOverDelay ) );
            else {
                StartCoroutine( GameOver( win, 0f ) );
            }
        }
    }

    IEnumerator GameOver(bool win, float delay) {
        if(win) {
            int reward = levelRewardCash * Timer.instance.score;
            ShopManager.instance.AddCash( reward );
        }else {
            gameFailed = true;
            print( "You FAILED young padawan..." );
        }

        yield return new WaitForSeconds( delay );

        gameLevels[currentLevel].SetActive( false );

        Time.timeScale = 0f;

        this.win = win;
        inGame = false;
        continueShown = false;

        if(win) {
            if( winSound != null && !settings.muteFX )
                winSoundSource.Play();

            SaveWonLevel(GUIController.instance.timer);
        }

        gameOverCount++;

        inGameSoundSource.Stop();
        //musicSource.Play();
        //musicSource.volume = musicVolumeMenu;

        OnGameEnd( win );
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------- LOAD LEVEL
    //--------------------------------------------------------------------------------------
    IEnumerator LoadLevel(int level) 
    {
        for( int l = 1 ;l < SceneManager.sceneCountInBuildSettings ;l++ ) {
            SceneManager.UnloadScene( l );
        }

        loading = true;
        WheelCtrl.instance.wheelBeingHeld = false;

        if(level == 1) {
            AsyncOperation async = SceneManager.LoadSceneAsync( currentLevel, LoadSceneMode.Additive );
            yield return async;
        } else {
            yield return new WaitForSeconds( 2f );
        }

        loading = false;
        inGame = true;

        VehicleManager.instance.GameStart();
        Timer.instance.ResetTimer();
        
        //VehicleManager.instance.currentVehicle.GetComponent<CarAudio>().SoundOn();

        OnGameStart();
        bl_MiniMap.instance.SetMinimapTarget();
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------- GAME START
    //--------------------------------------------------------------------------------------
    public void StartGame() {
        win = false;
        gameEnded = false;
        gamePaused = false;
        gameFailed = false;
        crashed = false;
        continueShown = false;

        GUIController.instance.breaking = false;
        GUIController.instance.steerLeft = false;
        GUIController.instance.steerRight = false;
        GUIController.instance.accelerating = false;

        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;

        checkpointController.Reset();

        //--------------------------------------------------------------------------------------
        for( int i=0 ; i < gameLevels.Count ; i++ ) {
            if(i == currentLevel) {
                gameLevels[i].SetActive( true );

                foreach(Transform gameObj in gameLevels[i].transform) {
                    gameObj.gameObject.SetActive( true );
                }

                checkpointController.gameLevels = gameLevels[i];
            } else {
                gameLevels[i].SetActive( false );
            }
        }
        //--------------------------------------------------------------------------------------

        if( selectedMode == GameModes.Cadet || selectedMode == GameModes.Captain ) {
            crashProtectionsFromMode = -1;
        }

        if(selectedMode == GameModes.Corporal || selectedMode == GameModes.Sergeant) {
            if(settings.crashProtectionFirst)
                crashProtectionsFromMode = 1;

            if( crashProtectionsFromMode < 0 )
                crashProtectionsFromMode = 0;

            settings.crashProtectionFirst = false;
        }

        //--------------------------------------------------------------------------------------

        if( musicSource != null) {
            musicSource.Stop();
            inGameSoundSource.volume = musicVolumeGame;
            inGameSoundSource.Play();
        }

        //--------------------------------------------------------------------------------------

        StartCoroutine( LoadLevel( currentLevel ) );
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------- UNLOCKING LEVELS
    //--------------------------------------------------------------------------------------

    public void UnlockLevel(int level ) {
        levelsUnlocked[selectedCar][level] = 1;
        PlayerPrefsX.SetIntArray( "LevelsUnlockedCar" + selectedCar, levelsUnlocked[selectedCar] );
    }

    //--------------------------------------------------------------------------------------

    public void UnlockAllLevels() {
        for(int i=0 ; i<levelsUnlocked.Length ; i++ ) {
            for( int j = 0 ; j < levelsUnlocked[i].Length ; j++ ) {
                levelsUnlocked[i][j] = 1;
            }
            PlayerPrefsX.SetIntArray( "LevelsUnlockedCar" + i, levelsUnlocked[i] );
        }
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- SAVE AFTER WON
    //--------------------------------------------------------------------------------------
    [HideInInspector]
    public ParkingController parkingController;
    public void SaveWonLevel(Timer timer) {
        if(currentLevel < levelsUnlocked[selectedCar].Length - 1) {
            if(levelsUnlocked[selectedCar][currentLevel+1] == 0) {
                levelsUnlocked[selectedCar][currentLevel + 1] = 1;
                PlayerPrefsX.SetIntArray( "LevelsUnlockedCar"+selectedCar, levelsUnlocked[selectedCar] );
            }
        }

        if(levelStars[selectedCar][currentLevel] < timer.score) {
            levelStars[selectedCar][currentLevel] = timer.score;
            PlayerPrefsX.SetIntArray( "LevelStarsCar" + selectedCar, levelStars[selectedCar] );
        }

        parkingController = VehicleManager.instance.currentVehicle.GetComponent<ParkingController>();

        if( levelScores[selectedCar][currentLevel] < timer.score * ( 1000 - timer.time ) + ( 1 - parkingController.distance )*100 ) {
            levelScores[selectedCar][currentLevel] = (int)( timer.score * ( 1000 - timer.time ) + ( 1 - parkingController.distance )*100 );
            PlayerPrefsX.SetIntArray( "LevelScoresCar" + selectedCar, levelScores[selectedCar] );
        }

        if( levelTimes[selectedCar][currentLevel] > timer.time || levelTimes[selectedCar][currentLevel] == 0 ) {
            levelTimes[selectedCar][currentLevel] = timer.time;
            PlayerPrefsX.SetFloatArray( "LevelTimesCar" + selectedCar, levelTimes[selectedCar] );
        }
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------- TOTAL SCORE
    //--------------------------------------------------------------------------------------

    int TotalScore() {
        int score = 0;
        for(int i=0 ; i<levelScores.Length ; i++ ) {
            for( int j = 0 ;j < levelScores[i].Length ;j++ ) {
                score += levelScores[i][j];
            }
        }
        return score;
    }
}
