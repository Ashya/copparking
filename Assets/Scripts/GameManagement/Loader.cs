﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Loader : MonoBehaviour {

    private bool loadScene = false;

    [SerializeField]
    private int scene;

    [SerializeField]
    private GameObject loadingPanel;

    void Update() {
        if(Input.GetKey(KeyCode.Space) && loadScene == false) {
            loadScene = true;

            loadingPanel.SetActive( true );

            StartCoroutine(LoadNewScene());
        }

        if(!loadScene)
            loadingPanel.SetActive( false );
    }

    IEnumerator LoadNewScene() {
        yield return new WaitForSeconds( 3 );

        AsyncOperation async = Application.LoadLevelAsync( scene );

        while( !async.isDone ) {
            yield return null;
        }
    }

}
