﻿using UnityEngine;
using System.Collections;

public class CollisionRespawn : MonoBehaviour {
    public enum RespawnTypes { spawnPoint, checkpoint }
    public RespawnTypes respawnType = RespawnTypes.spawnPoint;
}
