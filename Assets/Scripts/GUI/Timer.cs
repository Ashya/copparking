﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : SingletonBehaviour<Timer> {

    [Header("TIMER TEXT COMPONENT")]
    public Text timerText;

    [Header("DISPLAY TIMES")]
    public float time;
    private int roundedSeconds;
    public int displaySeconds;
    public int displayMinutes;

    [Header("STAR COUNT")]
    public int score = 3;

    [Header("LEVELS TIMES")]
    public float normalTime = 25;
    public float mediumTime = 20;
    public float hardTime = 15;

    private float _additionalTime = 0;
    private float _targetTime;

    [Header("TIME OFFSET FOR EACH STAR")]
    public float twoStarsOffset = 15;
    public float oneStarOffset = 35;

    void Awake() {
        GameManager.OnGameStart += this.Reset;
        GUIController.instance.timer = this;
        //Reset();
    }

    void Start() {
        time = 0;
    }

    void OnGameEnd(bool win) {
        enabled = false;
    }

    public void ResetTimer() {
        time = 0;
        score = 3;

        switch( GameManager.instance.selectedMode ) {
            case GameManager.GameModes.Cadet:
                _additionalTime = normalTime;
                break;
            case GameManager.GameModes.Corporal:
                _additionalTime = mediumTime;
                break;
            case GameManager.GameModes.Sergeant:
                _additionalTime = mediumTime;
                break;
            case GameManager.GameModes.Captain:
                _additionalTime = hardTime;
                break;
        }

        enabled = true;
    }

    void Reset() {
        time = 0;
        score = 3;

        switch(GameManager.instance.selectedMode) {
            case GameManager.GameModes.Cadet:
                _additionalTime = normalTime;
                break;
            case GameManager.GameModes.Corporal:
                _additionalTime = mediumTime;
                break;
            case GameManager.GameModes.Sergeant:
                _additionalTime = mediumTime;
                break;
            case GameManager.GameModes.Captain:
                _additionalTime = hardTime;
                break;
        }

        enabled = true;
    }

    void Update() {
        if( GameManager.instance.gameEnded )
            return;

        if( GameManager.instance.inGame) {

            time += Time.deltaTime;
            roundedSeconds = Mathf.CeilToInt( (int)time );
            displaySeconds = roundedSeconds % 60;
            displayMinutes = roundedSeconds / 60;

            _targetTime = GameManager.instance.targetTimes[GameManager.instance.currentLevel] + _additionalTime;

            if( time >= _targetTime )
                score = 2;

            if( time >= _targetTime + twoStarsOffset )
                score = 1;

            if( time >= _targetTime + oneStarOffset )
                score = 0;

            timerText.text = string.Format( "{0:00}:{1:00}", displayMinutes, displaySeconds );
        }
    }
}
