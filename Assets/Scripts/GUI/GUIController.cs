﻿using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

//---------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------- [CLASS] GUIController
//---------------------------------------------------------------------------------------------------------------------------------------------

public class GUIController : SingletonBehaviour<GUIController> {

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- VARIABLES
    //--------------------------------------------------------------------------------------
    static bool created = false;

    int helpIndex = 0;
    GameObject currentMenu;

    [HideInInspector]
    public Timer timer;

    [Separator("Game Panels")]
    public GameObject mainPanel;
    public GameObject levelSelectPanel;
    public GameObject carSelectPanel;
    public GameObject settingsPanel;
    public GameObject pausePanel;
    public GameObject crashPanel;
    public GameObject summaryPanel;
    public GameObject failPanel;
    public GameObject unlockLevelPanel;
    public GameObject inGamePanel;
    public GameObject tutorialPanel;
    public GameObject unlockCarPanel;

    [Separator("Vehicles lock buttons")]
    public GameObject[] carButtons;

    [Separator("Player's wallet")]
    public GameObject walletPanel;
    public Text cashText;

    [Separator("Game prices texts")]
    public Text vehiclePrice1;
    public Text vehiclePrice2;
    public Text vehiclePrice3;

    [Separator("Summary panel")]
    public Text timeText;
    public Text scoreText;
    public Text rewardText;

    [Separator("Shop panels")]
    public GameObject shopPanel;
    public GameObject notEnoughCashPanel;
    public GameObject vehicleBoughtPanel;
    public GameObject levelUnlockedPanel;

    [Separator("Minimap")]
    [Space(10)]
    public GameObject miniMap;
    public Text checkpointAmount;

    [Separator("Game panels")]
    public GameObject leftButton;
    public GameObject rightButton;
    public GameObject breakButton;
    public GameObject accelerateButton;
    public GameObject steeringWheelButton;

    [Separator("Level info objects")]
    public GameObject levelInfoPopUp;
    public GameObject notParkedPopUp;
    public Text levelInfoText;

    public GameObject nextLevelButton;

    public Transform levelGrid;
    List<Level> levels = new List<Level>();

    [Separator("Level loading")]
    public GameObject loadingPanel;

    [Separator("UI booleans")]
    public bool accelerating = false;
    public bool breaking = false;
    public bool steerLeft = false;
    public bool steerRight = false;

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------ CAR CONTROLLS
    //--------------------------------------------------------------------------------------
    public void Accelerate() {
        accelerating = true;
        breaking = false;
    }

    public void Break() {
        accelerating = false;
        breaking = true;
    }

    public void Right() {
        steerLeft = false;
        steerRight = true;
    }

    public void Left() {
        steerLeft = true;
        steerRight = false;
    }

    public void ClearInput() {
        accelerating = false;
        breaking = false;
    }

    public void ClearSteerInput() {
        steerRight = false;
        steerLeft = false;
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------- LEVEL GRID
    //--------------------------------------------------------------------------------------
    internal class Level
    {
        public int ID;
        public GameObject parent;
        public GameObject lockSprite;
        public GameObject starsContainer;

        public Text score;
        public GameObject scoreObject;
        public List<GameObject> stars = new List<GameObject>();
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- EMPTY DELEGATE
    //--------------------------------------------------------------------------------------
    delegate void VoidDelegate();
    VoidDelegate backAction = null;

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------- AWAKE
    //--------------------------------------------------------------------------------------
    void Awake() {
        if(!created) {
            created = true;
            DontDestroyOnLoad( gameObject );
            _instance = this;

            GameManager.OnGameStart += this.OnGameStart;
            GameManager.OnGameEnd += this.OnGameEnd;
            GameManager.OnShowContinue += this.OnShowContinue;
            GameManager.OnResumeFromContinue += this.OnResumeFromContinue;
        } else {
            DestroyImmediate( gameObject );
        }
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------- START
    //--------------------------------------------------------------------------------------
    void Start() {
        DisableAll();

        int idTmp = 0;
        foreach(Transform levelInGrid in levelGrid) {
            Level level = new Level();
            level.ID = idTmp;
            idTmp++;

            level.parent = levelInGrid.gameObject;
            level.score = levelInGrid.FindChild( "Label - Score" ).GetComponent<Text>();
            level.scoreObject = levelInGrid.FindChild( "Label - Score" ).gameObject;
            level.lockSprite = levelInGrid.FindChild("Label").FindChild( "Panel" ).gameObject;
            level.starsContainer = levelInGrid.FindChild( "Stars" ).gameObject;

            foreach(Transform star in levelInGrid.FindChild("Stars")) {
                level.stars.Add( star.gameObject );
            }

            level.stars.Sort( ( x, y ) => string.Compare( x.name, y.name ) );
            levels.Add( level );
        }

        levels.Sort( ( x, y ) => string.Compare( x.parent.name, y.parent.name ) );

        EnterMainPanel();
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------- GAME START
    //--------------------------------------------------------------------------------------
    void OnGameStart() {       
        DisableAll();

        inGamePanel.SetActive( true );
        miniMap.SetActive( true );

        StartCoroutine( ShowLevelInfo(GameManager.instance.currentLevel) );

        if( GameManager.instance.settings.buttonSteering ) {
            leftButton.SetActive( true );
            rightButton.SetActive( true );
            steeringWheelButton.SetActive( false );
        }

        if( GameManager.instance.settings.wheelSteering ) {
            steeringWheelButton.SetActive( true );
            leftButton.SetActive( false );
            rightButton.SetActive( false );
        }

        if( GameManager.instance.settings.tiltSteering ) {
            steeringWheelButton.SetActive( false );
            leftButton.SetActive( false );
            rightButton.SetActive( false );
        }
    }

    IEnumerator ShowLevelInfo( int _currentLevel ) {
        levelInfoText.text = "Level " + _currentLevel;

        levelInfoPopUp.SetActive( true );
        yield return new WaitForSeconds( 5 );
        levelInfoPopUp.SetActive( false );
    }

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- GAME OVER
    //--------------------------------------------------------------------------------------
    void OnGameEnd(bool win) {
        if( win )
            EnterSummaryPanel();
        else {
            if( GameManager.instance.crashed )
                EnterFailPanel();
            else
                print("NOT CRASHED");
        }
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------ CONTINUE SHOW
    //--------------------------------------------------------------------------------------
    void OnShowContinue() {

    }

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------- RESUME AFTER CONTINUE
    //--------------------------------------------------------------------------------------
    void OnResumeFromContinue() {

    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- RESUME / PAUSE
    //--------------------------------------------------------------------------------------
    public void Resume() {
        GameManager.instance.Resume();

        pausePanel.SetActive( false );
        inGamePanel.SetActive( true );
        miniMap.SetActive( true );

        backAction = Pause;
    }

    public void Pause() {
        GameManager.instance.Pause();
        currentMenu = pausePanel;

        pausePanel.SetActive( true );
        inGamePanel.SetActive( false );
        miniMap.SetActive( false );

        backAction = Resume;
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- DISABLE PANELS
    //--------------------------------------------------------------------------------------
    void DisableAll() {
        shopPanel.SetActive( false );
        failPanel.SetActive( false );
        mainPanel.SetActive( false );
        crashPanel.SetActive( false );
        pausePanel.SetActive( false );
        inGamePanel.SetActive( false );
        summaryPanel.SetActive( false );
        settingsPanel.SetActive( false );
        carSelectPanel.SetActive( false );
        unlockCarPanel.SetActive( false );
        unlockLevelPanel.SetActive( false );
        levelSelectPanel.SetActive( false );

        miniMap.SetActive( false );
        loadingPanel.SetActive( false );
        tutorialPanel.SetActive( false );
        levelInfoPopUp.SetActive( false );
        nextLevelButton.SetActive( false );

        notEnoughCashPanel.SetActive( false );
        vehicleBoughtPanel.SetActive( false );
        levelUnlockedPanel.SetActive( false );

        notParkedPopUp.SetActive( false );
        walletPanel.SetActive( false );
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------- ENTER MAIN PANEL
    //--------------------------------------------------------------------------------------
    public void EnterMainPanel() {
        DisableAll();

        walletPanel.SetActive( true );

        GameManager.instance.inGame = false;
        GameManager.instance.gameEnded = false;
        GameManager.instance.win = false;

        if( GameManager.instance.gameLevels != null )
            GameManager.instance.gameLevels[GameManager.instance.currentLevel].SetActive( false );

        for( int l = 1 ;l < Application.levelCount ;l++ ) {
            Application.UnloadLevel( l );
        }

        if( VehicleManager.instance.currentVehicle != null ) {
            Destroy( VehicleManager.instance.currentVehicle.gameObject );
            VehicleManager.instance.currentVehicle = null;
        }

        mainPanel.SetActive( true );
        currentMenu = mainPanel;

#if UNITY_ANDROID
        backAction = Application.Quit;
#endif
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------- ENTER PAUSE PANEL
    //--------------------------------------------------------------------------------------
    public void EnterPausePanel() {
        DisableAll();
        pausePanel.SetActive( true );
        currentMenu = pausePanel;
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------- ENTER CAR SELECT PANEL
    //--------------------------------------------------------------------------------------
    public void PrepareCarSelect() {
        for( int i=0 ; i< GameManager.instance.carsUnlocked.Length ; i++ ) {
            if( GameManager.instance.carsUnlocked[i] == 1 )
                carButtons[i].SetActive( false );
            else
                carButtons[i].SetActive( true );
        }
    }

    public void EnterCarSelect() {
        DisableAll();

        PrepareCarSelect();
        currentMenu = carSelectPanel;
        backAction = EnterMainPanel;

        walletPanel.SetActive( true );
        carSelectPanel.SetActive( true );
    }

    //--------------------------------------------------------------------------------------
    int selectedCar = -1;
    public void SelectCar() {
        GameObject selected = GetSelectedItem();

        string name = selected.name;
        selectedCar = int.Parse( name[name.Length - 1].ToString() );

        if( GameManager.instance.carsUnlocked[selectedCar] == 1 ) {
            GameManager.instance.selectedCar = selectedCar;
            EnterLevelSelect();
        }else {
            BuyCarShow();
        }
    }

    //--------------------------------------------------------------------------------------

    public void BuyCarShow() {
        DisableAll();
        unlockCarPanel.SetActive( true );
    }

    public void BuyCarHide() {
        DisableAll();
        EnterCarSelect();
    }

    //--------------------------------------------------------------------------------------

    public void BuyCar() {
        //DisableAll();

        if( ShopManager.instance.BuyItem(ShopManager.instance.vehiclePrice) ) {
            PlayerPrefs.SetInt( "CarUnlocked" + selectedCar, 1 );
            GameManager.instance.carsUnlocked[selectedCar] = PlayerPrefs.GetInt( "CarUnlocked" + selectedCar );

            ShowVehicleBought();
        } else {
            ShowNotEnoughtCash();
        }
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------- BUY ITEMS POPUPS
    //--------------------------------------------------------------------------------------

    void ShowNotEnoughtCash() {
        notEnoughCashPanel.SetActive( true );
    }

    void ShowVehicleBought() {
        vehicleBoughtPanel.SetActive( true );
    }

    void ShowLevelUnlocked() {
        levelUnlockedPanel.SetActive( true );
    }

    public void HideVehicleBought() {
        HideInfoPopups();
        EnterCarSelect();
    }

    public void HideLevelBought() {
        HideInfoPopups();
        EnterLevelSelect();
    }

    public void HideInfoPopups() {
        notEnoughCashPanel.SetActive( false );
        vehicleBoughtPanel.SetActive( false );
        levelUnlockedPanel.SetActive( false );
    }    

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------- LEVELS
    //--------------------------------------------------------------------------------------
    public void EnterLevelSelect() {
        DisableAll();

        PrepareLevelSelection();

        currentMenu = levelSelectPanel;
        backAction = EnterCarSelect;

        walletPanel.SetActive( true );
        levelSelectPanel.SetActive( true );
    }

    //-------------------------------------------------------------------------------------- PREPARE LEVEL GRID
    void PrepareLevelSelection() {
        //From 1, because we don't want to see stars and score from tutorial
        for(int i=1 ; i<levels.Count ; i++ ) {
            if(GameManager.instance.levelsUnlocked[GameManager.instance.selectedCar][i] == 1) {
                levels[i].lockSprite.SetActive( false );

                //levels[i].parent.GetComponent<Button>().enabled = true;
                levels[i].scoreObject.SetActive( true );
                levels[i].starsContainer.SetActive( true );
                levels[i].score.text = GameManager.instance.levelScores[GameManager.instance.selectedCar][i].ToString();

                for(int s=0 ; s<levels[i].stars.Count ; s++ ) {
                    if( GameManager.instance.levelStars[GameManager.instance.selectedCar][i] > s )
                        levels[i].stars[s].SetActive( true );
                    else
                        levels[i].stars[s].SetActive( false );
                }
            }else {
                levels[i].scoreObject.SetActive( false );
                levels[i].starsContainer.SetActive( false );
                //levels[i].parent.GetComponent<Button>().enabled = false;

                levels[i].lockSprite.SetActive( true );
            }
        }
    }

    //-------------------------------------------------------------------------------------- SELECT LEVEL
    public void SelectLevel() {
        backAction = null;

        GameObject selected = GetSelectedItem();
        string name = selected.name;
        int selectedLevel = int.Parse(name[name.Length - 2].ToString() + name[name.Length - 1].ToString());

        if(GameManager.instance.levelsUnlocked[GameManager.instance.selectedCar][selectedLevel] == 1) {
            GameManager.instance.currentLevel = selectedLevel;

            /*StartCoroutine( ShowLoadingLevel() );

            levelSelectPanel.SetActive( false );
            inGamePanel.SetActive( true );
            miniMap.SetActive( true );*/

            levelSelectPanel.SetActive( false );
            GameManager.instance.StartGame();
        }else {
            ShowUnlockLevel(selectedLevel);
        }
    }

    //-------------------------------------------------------------------------------------- RETRY LEVEL
    public void RetryLevel() {
        backAction = null;
        DisableAll();

        int levelToLoad = GameManager.instance.currentLevel;
        inGamePanel.SetActive( true );
        miniMap.SetActive( true );

        GameManager.instance.StartGame();
    }

    //-------------------------------------------------------------------------------------- NEXT LEVEL
    public void GoToNextLevel() {
        backAction = null;
        DisableAll();

        int levelToLoad = GameManager.instance.currentLevel + 1;

        if( levelToLoad <= GameManager.instance.levels ) {
            if( GameManager.instance.levelsUnlocked[GameManager.instance.selectedCar][levelToLoad] == 1 ) {
                GameManager.instance.currentLevel = levelToLoad;
                inGamePanel.SetActive( true );
                miniMap.SetActive( true );
                GameManager.instance.StartGame();
            }
        }
    }

    //-------------------------------------------------------------------------------------- UNLOCK LEVEL
    int levelToUnlock = -1;
    public void ShowUnlockLevel(int level) {
        DisableAll();
        levelToUnlock = level;
        unlockLevelPanel.SetActive( true );
    }

    public void UnlockLevelAndHidePanel() {

        if(ShopManager.instance.BuyItem(ShopManager.instance.levelPrice)) {
            GameManager.instance.UnlockLevel( levelToUnlock );
            levelToUnlock = -1;

            ShowLevelUnlocked();
        }else {
            ShowNotEnoughtCash();
        }
        
    }

    public void CloseUnlockLevel() {
        unlockLevelPanel.SetActive( false );
        levelToUnlock = -1;
        EnterLevelSelect();
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------- GET SELECTED ITEM
    //--------------------------------------------------------------------------------------

    public GameObject GetSelectedItem() {
        return EventSystem.current.currentSelectedGameObject;
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------- CHECKPOINTS
    //--------------------------------------------------------------------------------------
    public void UpdateCheckpointAmount( int amount, int totalAmount ) {
        checkpointAmount.text = amount.ToString( "00" ) + "/" + totalAmount.ToString( "00" );
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- LOAD MAIN MENU
    //--------------------------------------------------------------------------------------
    
    public void LoadMainMenu() {
        backAction = null;

        Time.timeScale = 1.0f;
        StartCoroutine( LoadingBeforeMainMenu( 2f ) );

        GameManager.instance.inGameSoundSource.Stop();
        GameManager.instance.musicSource.Play();

        EnterMainPanel();
    }

    IEnumerator LoadingBeforeMainMenu(float sec) {
        GameManager.instance.loading = true;
        yield return new WaitForSeconds( sec );
        GameManager.instance.loading = false;
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------ ENTER SUMMARY PANEL
    //--------------------------------------------------------------------------------------

    public void EnterSummaryPanel() {
        Timer myTimer = Timer.instance;
        DisableAll();

        currentMenu = summaryPanel;
        backAction = EnterSummaryPanel;

        int points = (int)( myTimer.score * ( 1000 - myTimer.time ) + ( 1 - GameManager.instance.parkingController.distance ) * 100 );

        timeText.text = myTimer.timerText.text;
        scoreText.text = points.ToString();
        rewardText.text = ( GameManager.instance.levelRewardCash * myTimer.score ).ToString();

        summaryPanel.SetActive( true );

        if(GameManager.instance.currentLevel < GameManager.instance.levels - 1 ) {
            nextLevelButton.SetActive( true );
        }else {
            nextLevelButton.SetActive( false );
        }
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------- ENTER FAIL PANEL
    //--------------------------------------------------------------------------------------

    public void EnterFailPanel() {
        DisableAll();

        //GameManager.instance.gamePaused = true;
        //GameManager.instance.win = false;
        //GameManager.instance.gameFailed = true;
        //GameManager.instance.GameOver( GameManager.instance.win );

        currentMenu = failPanel;
        backAction = EnterFailPanel;

        failPanel.SetActive( true );
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------- ENTER SETTINGS PANEL
    //--------------------------------------------------------------------------------------

    public void EnterSettingsPanel() {
        DisableAll();
        currentMenu = settingsPanel;

        settingsPanel.SetActive( true );
    }

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------- ENTER SHOP PANEL
    //--------------------------------------------------------------------------------------

    public void EnterShopPanel() {
        DisableAll();
        currentMenu = shopPanel;

        shopPanel.SetActive( true );
        //walletPanel.SetActive( true );
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------ IN GAME INFOS
    //--------------------------------------------------------------------------------------

    public void ShowNotParkedInfo() {
        notParkedPopUp.SetActive( true );
        print( "Showing popup" );
    }

    public void HideNotParkedInfo() {
        notParkedPopUp.SetActive( false );
    }

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------- BACK ACTION
    //--------------------------------------------------------------------------------------
    public void Back() {
        DisableAll();

        if(GameManager.instance.inGame) {
            if(!GameManager.instance.gameEnded) {
                if(CrashController.instance.crashed) {
                    currentMenu = failPanel;
                    failPanel.SetActive( true );
                } else {
                    currentMenu = pausePanel;
                    pausePanel.SetActive( true );
                    backAction = Resume;
                }
            }else {
                if(GameManager.instance.win) {
                    EnterSummaryPanel();
                }else {
                    EnterFailPanel();
                }
            }
        }else {
            if( GameManager.instance.gameEnded ) {
                if( !GameManager.instance.gameFailed ) {
                    EnterSummaryPanel();
                } else {
                    EnterFailPanel();
                }
            } else {
                if( currentMenu == levelSelectPanel )
                    EnterCarSelect();
                else
                    EnterMainPanel();
            }
        }
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------ LOADING
    //--------------------------------------------------------------------------------------
    void Update() {
        if(GameManager.instance.loading) {
            loadingPanel.SetActive( true );
        }else {
            loadingPanel.SetActive( false );
        }

        if(!GameManager.instance.inGame) {
            cashText.text = ShopManager.instance._cash + " $";

            vehiclePrice1.text = ShopManager.instance.vehiclePrice.ToString() + " $";
            vehiclePrice2.text = vehiclePrice1.text;
            vehiclePrice3.text = vehiclePrice1.text;
        }
    }
}
