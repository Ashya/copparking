﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SettingsController : SingletonBehaviour<SettingsController> {

    private bool created = false;

    //--------------------------------------------------------------------------------
    //---------------------------------------------------------------------- VARIABLES
    //--------------------------------------------------------------------------------

    [Separator("Car steering")]
    public Toggle tiltToggle;
    public Toggle wheelToggle;
    public Toggle buttonsToggle;
    public Slider sensitivitySlider;

    [Space(10)]
    public Image tiltImage;
    public Image wheelImage;
    public Image buttonsImage;

    [Separator("Game audio")]
    public Toggle musicToggle;
    public Toggle effectsToggle;
    public Slider audioSlider;
    public Slider inGameAudioSlider;

    private Color32 activeColor;
    private Color32 inactiveColor;
    //--------------------------------------------------------------------------------
    //-------------------------------------------------------------------------- AWAKE
    //--------------------------------------------------------------------------------

    void Awake() {
        if(!created) {
            DontDestroyOnLoad( instance );
            _instance = this;

            GameManager.OnGameStart += OnGameStart;

            created = true;
        }else {
            DestroyImmediate( gameObject );
        }
    }

    //--------------------------------------------------------------------------------
    //--------------------------------------------------------------------------- INIT
    //--------------------------------------------------------------------------------

    public void InitController() {
        inactiveColor = new Color32( 33, 150, 243, 255 );
        activeColor = new Color32( 144, 202, 249, 255 );

        tiltToggle.isOn = GameManager.instance.settings.tiltSteering;
        wheelToggle.isOn = GameManager.instance.settings.wheelSteering;
        buttonsToggle.isOn = GameManager.instance.settings.buttonSteering;
        sensitivitySlider.value = GameManager.instance.settings.sensitivity;

        tiltImage = tiltToggle.GetComponentInChildren<Image>();
        wheelImage = wheelToggle.GetComponentInChildren<Image>();
        buttonsImage = buttonsToggle.GetComponentInChildren<Image>();

        musicToggle.isOn = !GameManager.instance.settings.muteMusic;
        effectsToggle.isOn = !GameManager.instance.settings.muteFX;
        audioSlider.value = GameManager.instance.settings.audioVolume;
        inGameAudioSlider.value = GameManager.instance.settings.inGameAudioVolume;

        tiltToggle.onValueChanged.AddListener( delegate { TiltSteering(); } );
        buttonsToggle.onValueChanged.AddListener( delegate { ButtonSteering(); } );
        wheelToggle.onValueChanged.AddListener( delegate { WheelSteering(); } );
        sensitivitySlider.onValueChanged.AddListener( delegate { SensitivityControlls(); } );

        musicToggle.onValueChanged.AddListener(delegate { MusicControlls(); });
        effectsToggle.onValueChanged.AddListener( delegate { EffectsControlls(); } );
        audioSlider.onValueChanged.AddListener( delegate { AudioVolumeControlls(); } );
        inGameAudioSlider.onValueChanged.AddListener( delegate { InGameAudioVolumeControlls(); } );
    }

    public void OnGameStart() {
    }

    //--------------------------------------------------------------------------------
    //-------------------------------------------------------------------------- AUDIO
    //--------------------------------------------------------------------------------

    public void MusicControlls() {
        GameManager.instance.settings.muteMusic = !musicToggle.isOn;
        GameManager.instance.musicSource.mute = !musicToggle.isOn;
        GameManager.instance.inGameSoundSource.mute = !musicToggle.isOn;
    }

    public void EffectsControlls() {
        GameManager.instance.settings.muteFX = !effectsToggle.isOn;
    }

    public void AudioVolumeControlls() {
        GameManager.instance.settings.audioVolume = audioSlider.value;
        GameManager.instance.musicVolumeMenu = audioSlider.value;
        GameManager.instance.musicSource.volume = audioSlider.value;
    }

    public void InGameAudioVolumeControlls() {
        GameManager.instance.settings.inGameAudioVolume = inGameAudioSlider.value;
        GameManager.instance.musicVolumeGame = inGameAudioSlider.value;
        GameManager.instance.inGameSoundSource.volume = inGameAudioSlider.value;
    }

    //--------------------------------------------------------------------------------
    //----------------------------------------------------------------------- STEERING
    //--------------------------------------------------------------------------------

    public void SensitivityControlls() {
        GameManager.instance.settings.sensitivity = (int)( sensitivitySlider.value * 10 );
    }

    public void ButtonSteering() {
        GameManager.instance.settings.buttonSteering = buttonsToggle.isOn;
    }

    public void TiltSteering() {
        GameManager.instance.settings.tiltSteering = tiltToggle.isOn;
    }

    public void WheelSteering() {
        GameManager.instance.settings.wheelSteering = wheelToggle.isOn;
    }
}