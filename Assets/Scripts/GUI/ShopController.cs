﻿using UnityEngine;
using System.Collections;

public class ShopController : SingletonBehaviour<ShopController> {

    private bool created = false;

	void Awake () {
        if( !created ) {
            DontDestroyOnLoad( instance );
            _instance = this;

            created = true;
        } else {
            DestroyImmediate( gameObject );
        }
	}

	public void RestorePurchases() {
        if( Application.isEditor ) {
            Debug.Log("[SHOP CONTROLLER] Purchases restore!");
        }
    }
}
