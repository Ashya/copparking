﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent( typeof( CarController ) )]
public class CarControlls : MonoBehaviour
{
    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- VARIABLES
    //--------------------------------------------------------------------------------------
    public enum CONTROLLS { Wheel, Tilt, Buttons }
    [Separator( "Controlls type" )]
    public CONTROLLS controllsType = CONTROLLS.Buttons;

    [Separator( "Variables" )]
    public Transform touchGuiContainer;
    public CarController carController;

    int sensitivity;

    float gasValue = 0;
    float lastGasValue;
    float breakValue = 0;
    float lastBreakValue;

    float accX = 0;
    float prevAccX;

    float steerValue = 0;

    float horizVal = 0;
    float prevHorizVal;

    float steeringWheelRotationSpeed = 3f;

    //--------------------------------------------------------------------------------------

    [Separator( "Settings" )]
    public float maximumAngle = 270f;
    public float wheelReleasedSpeed = 500f;

    float wheelAngle = 0f;
    float lastWheelAngle;

    public bool wheelIsHeld = false;

    //--------------------------------------------------------------------------------------

    bool created = false;
    bool steering = false;

    Camera cam;

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------- AWAKE
    //--------------------------------------------------------------------------------------
    void Awake() {
        if( !created ) {
            created = true;
            sensitivity = GameManager.instance.settings.sensitivity;

            cam = GameObject.FindGameObjectWithTag( "GuiCamera" ).GetComponent<Camera>();

            if( GameManager.instance.settings.buttonSteering )
                controllsType = CONTROLLS.Buttons;

            if( GameManager.instance.settings.tiltSteering )
                controllsType = CONTROLLS.Tilt;

            if( GameManager.instance.settings.wheelSteering )
                controllsType = CONTROLLS.Wheel;

            GameManager.OnGameStart += OnGameStart;
        } else {
            DestroyImmediate( gameObject );
        }
    }

    void OnGameStart() {
        carController = VehicleManager.instance.currentVehicle.GetComponent<CarController>();
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------- ACC/BREAK STEERING
    //--------------------------------------------------------------------------------------
    void AccelerationBreak() {
        if( GUIController.instance.accelerating ) {
            gasValue = Mathf.Lerp( lastGasValue, 1, Time.deltaTime * ( 2 + GameManager.instance.settings.sensitivity / 2 ) );
            lastGasValue = gasValue;
            lastBreakValue = 0;
        }

        if( GUIController.instance.breaking ) {
            breakValue = Mathf.Lerp( -1, lastBreakValue, -Time.deltaTime * ( 2 + GameManager.instance.settings.sensitivity / 2 ) );
            lastBreakValue = breakValue;
            lastGasValue = 0;
        }

        if(!GUIController.instance.breaking && !GUIController.instance.accelerating) {
            lastGasValue = 0;
            lastBreakValue = 0;
        }
    }

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------- BUTTON STEERING
    //--------------------------------------------------------------------------------------

    public void ButtonSteering() {
        if( GameManager.instance.settings.buttonSteering ) {
            if( GUIController.instance.steerLeft ) {
                horizVal -= steeringWheelRotationSpeed * Time.deltaTime * GameManager.instance.settings.sensitivity;
                horizVal = Mathf.Clamp( Mathf.Lerp( prevHorizVal, horizVal, Time.deltaTime * 10 ), -1f, 1f );

                prevHorizVal = horizVal;
                steerValue = horizVal;
            }

            if( GUIController.instance.steerRight ) {
                horizVal += steeringWheelRotationSpeed * Time.deltaTime * GameManager.instance.settings.sensitivity;
                horizVal = Mathf.Clamp( Mathf.Lerp( prevHorizVal, horizVal, Time.deltaTime * 10 ), -1f, 1f );

                prevHorizVal = horizVal;
                steerValue = horizVal;
            }

            if( !GUIController.instance.steerLeft && !GUIController.instance.steerRight ) {
                prevHorizVal = Mathf.Lerp( prevHorizVal, 0f, Time.deltaTime * steeringWheelRotationSpeed );
                steerValue = Mathf.Lerp( steerValue, 0f, Time.deltaTime * steeringWheelRotationSpeed );
            }
        }
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------ TILT STEERING
    //--------------------------------------------------------------------------------------

    public void TiltSteering() {
        if( GameManager.instance.settings.tiltSteering ) {
            accX = Input.acceleration.x * GameManager.instance.settings.sensitivity;
            accX = Mathf.Clamp( Mathf.Lerp( prevAccX, accX, Time.deltaTime * 10 ), -1f, 1f );

            if( accX != 0 )
                steering = true;
            else
                steering = false;

            prevAccX = accX;

            if( !steering ) {
                if( Mathf.Abs( steerValue ) > 0.1f ) {
                    accX -= steeringWheelRotationSpeed * Time.deltaTime * Mathf.Sign( accX );
                } else {
                    accX = 0;
                }
            }

            steerValue = accX;
        }
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------- WHEEL STEERING
    //--------------------------------------------------------------------------------------
    public void ApplyWheelRotation(float value) {
        if(GameManager.instance.settings.wheelSteering) {
            steerValue = value;
        }
    }

    //--------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------- STEERING
    //--------------------------------------------------------------------------------------
    public void SteeringUpdate() {
        gasValue = 0;
        breakValue = 0;

        GameObject touchedObject = EventSystem.current.currentSelectedGameObject;

        AccelerationBreak();

        ButtonSteering();
        TiltSteering();

        carController.Move( steerValue, lastGasValue, lastBreakValue, 0 );
    }

    //--------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------- UPDATE
    //--------------------------------------------------------------------------------------
    void Update() {
        SteeringUpdate();        
    }
}