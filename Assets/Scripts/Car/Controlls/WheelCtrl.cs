﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;

public class WheelCtrl : SingletonBehaviour<WheelCtrl>
{
    private bool created = false;

    //--------------------------------------------------------------------------------------

    [Separator("SteeringWheel graphics")]
    public Graphic UI_Element;

    RectTransform rectT;
    Vector2 centerPoint;

    [Separator("Main settings")]
    public float maximumSteeringAngle = 270f;
    public float wheelReleasedSpeed = 500f;

    float wheelAngle = 0f;
    float wheelPrevAngle = 0f;

    public bool wheelBeingHeld = false;

    //--------------------------------------------------------------------------------------

    public float GetClampedValue() {
        return wheelAngle / maximumSteeringAngle;
    }

    //--------------------------------------------------------------------------------------

    public float GetAngle() {
        return wheelAngle;
    }

    //--------------------------------------------------------------------------------------

    void Start() {
        if(!created) {
            DontDestroyOnLoad( instance );
            _instance = this;
            created = true;

            rectT = UI_Element.rectTransform;

            InitEventsSystem();
            UpdateRect();
        }else {
            DestroyImmediate( gameObject );
        }
    }

    //--------------------------------------------------------------------------------------

    void Update() {
        if(GameManager.instance.inGame && GameManager.instance.settings.wheelSteering) {
            if( !wheelBeingHeld && !Mathf.Approximately( 0f, wheelAngle ) ) {
                float deltaAngle = wheelReleasedSpeed * Time.deltaTime;
                if( Mathf.Abs( deltaAngle ) > Mathf.Abs( wheelAngle ) )
                    wheelAngle = 0f;
                else if( wheelAngle > 0f )
                    wheelAngle -= deltaAngle;
                else
                    wheelAngle += deltaAngle;
            }

            rectT.localEulerAngles = Vector3.back * wheelAngle;
            VehicleManager.instance.currentVehicle.GetComponent<CarControlls>().ApplyWheelRotation( GetClampedValue() );
        }
    }

    //--------------------------------------------------------------------------------------

    void InitEventsSystem() {
        EventTrigger events = UI_Element.gameObject.GetComponent<EventTrigger>();

        if( events == null )
            events = UI_Element.gameObject.AddComponent<EventTrigger>();

        if( events.triggers == null )
            events.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();

        EventTrigger.Entry entry = new EventTrigger.Entry();
        EventTrigger.TriggerEvent callback = new EventTrigger.TriggerEvent();
        UnityAction<BaseEventData> functionCall = new UnityAction<BaseEventData>( PressEvent );
        callback.AddListener( functionCall );
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback = callback;

        events.triggers.Add( entry );

        entry = new EventTrigger.Entry();
        callback = new EventTrigger.TriggerEvent();
        functionCall = new UnityAction<BaseEventData>( DragEvent );
        callback.AddListener( functionCall );
        entry.eventID = EventTriggerType.Drag;
        entry.callback = callback;

        events.triggers.Add( entry );

        entry = new EventTrigger.Entry();
        callback = new EventTrigger.TriggerEvent();
        functionCall = new UnityAction<BaseEventData>( ReleaseEvent );//
        callback.AddListener( functionCall );
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback = callback;

        events.triggers.Add( entry );
    }

    //--------------------------------------------------------------------------------------

    void UpdateRect() {
        Vector3[] corners = new Vector3[4];
        rectT.GetWorldCorners( corners );

        for( int i = 0 ;i < 4 ;i++ ) {
            corners[i] = RectTransformUtility.WorldToScreenPoint( null, corners[i] );
        }

        Vector3 bottomLeft = corners[0];
        Vector3 topRight = corners[2];
        float width = topRight.x - bottomLeft.x;
        float height = topRight.y - bottomLeft.y;

        Rect _rect = new Rect( bottomLeft.x, topRight.y, width, height );
        centerPoint = new Vector2( _rect.x + _rect.width * 0.5f, _rect.y - _rect.height * 0.5f );
    }

    //--------------------------------------------------------------------------------------

    public void PressEvent( BaseEventData eventData ) {
        Vector2 pointerPos = ( (PointerEventData)eventData ).position;

        wheelBeingHeld = true;
        wheelPrevAngle = Vector2.Angle( Vector2.up, pointerPos - centerPoint );
    }

    //--------------------------------------------------------------------------------------

    public void DragEvent( BaseEventData eventData ) {
        Vector2 pointerPos = ( (PointerEventData)eventData ).position;

        float wheelNewAngle = Vector2.Angle( Vector2.up, pointerPos - centerPoint );

        if( Vector2.Distance( pointerPos, centerPoint ) > 20f ) {
            if( pointerPos.x > centerPoint.x )
                wheelAngle += wheelNewAngle - wheelPrevAngle;
            else
                wheelAngle -= wheelNewAngle - wheelPrevAngle;
        }

        wheelAngle = Mathf.Clamp( wheelAngle, -maximumSteeringAngle, maximumSteeringAngle );
        wheelPrevAngle = wheelNewAngle;
    }

    //--------------------------------------------------------------------------------------

    public void ReleaseEvent( BaseEventData eventData ) {
        DragEvent( eventData );
        wheelBeingHeld = false;
    }
}
