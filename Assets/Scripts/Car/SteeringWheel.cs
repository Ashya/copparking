﻿using UnityEngine;
using System.Collections;

//---------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------- [CLASS] STEERING WHEEL
//---------------------------------------------------------------------------------------------------------------------------------------------
public class SteeringWheel : SingletonBehaviour<SteeringWheel> {

    //--------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------- VARIABLES
    //--------------------------------------------------------------------------------------
    static bool created = false;
    
    public enum RotationAxix { X, Y, Z }
    [Header("ROTATE AROUND AXIS:")]
    public RotationAxix rotaionAroud = RotationAxix.Z;

    [Header("SETTINGS:")]
    [Range(10,100)]
    public int smoothing = 10;
    public float wheelRotations = 1.5f;

    [Space(10)]
    public bool reverseRotation = true;

    //--------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------- AWAKE
    //--------------------------------------------------------------------------------------
    void Awake () {
        if(!created) {
            DontDestroyOnLoad( instance );
            created = true;
            _instance = this;
        }else {
            DestroyImmediate( gameObject );
        }
	}

    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------- MOVE
    //--------------------------------------------------------------------------------------
    public void Move(float rot) {
        rot = Mathf.Clamp( rot, -1, 1 );

        if( reverseRotation )
            rot = -rot;

        float rotateAround = rot * 360f * wheelRotations;
        rotateAround = Mathf.Clamp( rotateAround, -360 * wheelRotations, 360 * wheelRotations );

        switch( rotaionAroud ) {
            case RotationAxix.X:
                transform.localEulerAngles = new Vector3( rotateAround, transform.localEulerAngles.y, transform.localEulerAngles.z );
                break;
            case RotationAxix.Y:
                transform.localEulerAngles = new Vector3( transform.localEulerAngles.x, rotateAround, transform.localEulerAngles.z );
                break;
            case RotationAxix.Z:
                transform.localEulerAngles = new Vector3( transform.localEulerAngles.x, transform.localEulerAngles.y, rotateAround );
                break;
        }
    }
}
