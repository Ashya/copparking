﻿using UnityEngine;
using System.Collections;

public class CrashController : SingletonBehaviour<CrashController> {

    private bool created = false;
    private CarController controller;

    [Separator("Crash settings")]
    [Range(0,10)]
    public float crashSpeed = 1f;

    [Separator("Crash booleans")]
    public bool crashed = false;
	
	void Awake () {
        if(!created) {
            DontDestroyOnLoad( instance );
            _instance = this;

            created = true;
            GameManager.OnGameStart += OnGameStart;
        }else {
            DestroyImmediate( gameObject );
        }
	}
	
	void OnGameStart() {
        crashed = false;
        controller = VehicleManager.instance.currentVehicle.GetComponent<CarController>();
    }

    void OnCollisionEnter(Collision collision) {
        if( !crashed && controller.CurrentSpeed >= crashSpeed ) {
            crashed = true;

            GameManager.instance.crashed = true;

            if( GameManager.instance.crashSound != null && !GameManager.instance.settings.muteFX ) {
                GameManager.instance.crashSoundSource.Play();
            }

            GUIController.instance.accelerating = false;
            GUIController.instance.breaking = false;
            GUIController.instance.steerLeft = false;
            GUIController.instance.steerRight = false;

            //GUIController.instance.EnterFailPanel();
            GameManager.instance.GameOver( false );
        }
    }
}
