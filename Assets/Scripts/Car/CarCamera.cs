﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CarCamera : MonoBehaviour
{

    // The target we are following
    //[HideInInspector]
    public Transform target;

    [Header("CAMERA SETTINGS")]
    // The distance in the x-z plane to the target
    public float distance = 10.0f;
    // the height we want the camera to be above the target
    public float height = 5.0f;
    // Look above the target (height * this ratio)
    public float topCameraOffset = 30f;
    public float targetHeightRatio = 0.5f;
    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;

    // Default settings
    float defaultDistance = 10.0f;
    float defaultHeight = 5.0f;
    float defaultTopCameraOffset = 30f;
    float defaultTargetHeightRatio = 0.5f;
    float defaultHeightDamping = 2.0f;
    float defaultRotationDamping = 3.0f;

    public float wantedRotationAngle = 0;
    [HideInInspector]
    public bool autoRotate = false;

    //for planes only
    public float minHeight = 2f;
    float defaultMinHeight = 2f;

    float lastWantedRotation;

    Vector3 tmp_vec;
    RaycastHit[] hits;
    RaycastHit hit;

    Vector3 rayDirection;
    float rayDistance;

    public Shader defaultShader;
    public Shader transparentShader;

    List<Renderer> obstructedObjects = new List<Renderer>();
    List<Renderer> lastObstructedObjects = new List<Renderer>();

    Renderer tmpRenderer;
    Vector3 p1;
    Vector3 p2;
    float capsuleSize = 1f;
    int layerMask { get { return TagManager.obstacleLayer; } }
    public bool frontCarCamera = false;
    public Transform vehicleFront;
    public bool topCarCamera = false;
    public bool obstacleOpaque = true;
    public bool renderVolume = false;

    float currentHeight;
    Quaternion currentRotation;

    private float shake_decay = 0;
    private float shake_intensity = 0;
    float defaultFOV;

    Vector3 right = Vector3.zero;
    bool gameEnded = false;
    public enum GameOverCameraTypes { crash, passThrough, normal }
    GameOverCameraTypes gameOverCameraType;

    void Awake() {
        GameManager.OnGameStart += this.OnGameStart;
        GameManager.OnGameEnd += this.OnGameEnd;
        GameManager.OnResumeFromContinue += this.OnResumeFromContinue;
        defaultFOV = Camera.main.fieldOfView;
        gameEnded = false;
        enabled = false;

        // Set defaults
        defaultDistance = distance;
        defaultHeight = height;
        defaultTopCameraOffset = topCameraOffset;
        defaultTargetHeightRatio = targetHeightRatio;
        defaultHeightDamping = heightDamping;
        defaultRotationDamping = rotationDamping;
        defaultMinHeight = minHeight;
    }

    void OnGameStart() {
        gameEnded = false;
        frontCarCamera = false;
        topCarCamera = false;
        autoRotate = true;

        target = VehicleManager.instance.currentVehicle.transform;

        ResetCameraPosition();
        StartCoroutine( CameraFix() ); //super magic fix for f*@#!g video ads and nextpeer

        enabled = true;
    }
    void OnResumeFromContinue() {
        gameEnded = false;
        StartCoroutine( CameraFix() );
    }

    IEnumerator CameraFix() {
        Camera camera = Camera.main;
        camera.enabled = false;
        yield return new WaitForSeconds( 0.1f );
        camera.enabled = true;
    }

    public void SwitchToGameOverCamera( GameOverCameraTypes type ) {
        gameOverCameraType = type;
        gameEnded = true;
    }

    public void ResetCameraPosition() {
        if( !topCarCamera ) {
            transform.position = target.position;
            transform.position -= currentRotation * Vector3.forward * distance;
            tmp_vec.x = transform.position.x;
            tmp_vec.y = currentHeight;
            tmp_vec.z = transform.position.z;

            transform.position = tmp_vec;
            transform.LookAt( target.position + Vector3.up * targetHeightRatio );
        }
    }

    public void ToggleCameraMode() {
        if( frontCarCamera ) {
            frontCarCamera = false;
            autoRotate = true;
        } else {
            frontCarCamera = true;
            autoRotate = false;
        }
    }

    void OnGameEnd( bool win ) {
        enabled = false;
    }

    void Update() {
        if(GameManager.instance.inGame) {
            if( !obstacleOpaque )
                return;

            lastObstructedObjects = new List<Renderer>( obstructedObjects );
            obstructedObjects.Clear();
            rayDirection = transform.position - target.position;
            rayDistance = Vector3.Distance( target.position, transform.position ) - 2f;

            p1 = target.position + transform.up * 0.5f;
            p2 = target.position + transform.up * 0.5f;
            hits = Physics.CapsuleCastAll( p1, p2, capsuleSize, rayDirection, rayDistance, 1 << layerMask );

            if( renderVolume )
                RenderVolume( p1, p2, capsuleSize, rayDirection, rayDistance );

            int i = 0;

            while( i < hits.Length ) {
                hit = hits[i];
                tmpRenderer = hit.collider.gameObject.GetComponent<Renderer>();

                if( tmpRenderer != null ) {
                    obstructedObjects.Add( tmpRenderer );
                    if( tmpRenderer ) {
                        foreach( Material material in tmpRenderer.materials ) {
                            if( material.shader == defaultShader )
                                material.shader = transparentShader;
                        }
                    }
                }
                i++;
            }

            foreach( Renderer renderer in lastObstructedObjects.Where( r => !obstructedObjects.Contains( r ) ).ToList() ) {
                foreach( Material material in renderer.materials ) {
                    if( material.shader == transparentShader )
                        material.shader = defaultShader;
                }
            }
        }
    }

    Transform shape;
    Material mat = null;

    void RenderVolume( Vector3 p1, Vector3 p2, float radius, Vector3 dir, float distance ) {
        if( !shape ) { // if shape doesn't exist yet, create it
            shape = GameObject.CreatePrimitive( PrimitiveType.Cube ).transform;
            Destroy( shape.GetComponent<Collider>() ); // no collider, please!
            shape.GetComponent<Renderer>().material = mat; // assign the selected material to it
        }
        Vector3 scale; // calculate desired scale
        float diam = 2 * radius; // calculate capsule diameter
        scale.x = diam; // width = capsule diameter
        scale.y = Vector3.Distance( p2, p1 ) + diam; // capsule height
        scale.z = distance + diam; // volume length
        shape.localScale = scale; // set the rectangular volume size
        // set volume position and rotation
        shape.position = ( p1 + p2 + dir.normalized * distance ) / 2;
        shape.rotation = Quaternion.LookRotation( dir, p2 - p1 );
        shape.GetComponent<Renderer>().enabled = true; // show it
    }

    void LateUpdate() {
        if( !target )
            return;

        MagicCamera();
    }

    void FixedUpdate() {
        if( GameManager.instance.inGame ) {
            if( shake_intensity > 0 ) {
                transform.position = transform.position + Random.insideUnitSphere * shake_intensity;
                transform.rotation = new Quaternion(
                                transform.rotation.x + Random.Range( -shake_intensity, shake_intensity ) * .2f,
                                transform.rotation.y + Random.Range( -shake_intensity, shake_intensity ) * .2f,
                                transform.rotation.z + Random.Range( -shake_intensity, shake_intensity ) * .2f,
                                transform.rotation.w + Random.Range( -shake_intensity, shake_intensity ) * .2f );
                shake_intensity -= shake_decay;
            }

            if( !gameEnded ) {
                if( frontCarCamera ) {
                    transform.position = vehicleFront.position;
                    transform.rotation = vehicleFront.rotation;
                } else {
                    right = target.right;
                    right.y = 0;

                    transform.position = Vector3.Slerp( transform.position, target.position - Vector3.Cross( right, target.forward ) * height - target.forward * distance, Time.deltaTime * 3 );

                    if( transform.position.y < minHeight ) {
                        transform.position = Vector3.Slerp( transform.position, new Vector3( transform.position.x, minHeight, transform.position.z ), Time.deltaTime * 30 );
                    }
                    transform.rotation = Quaternion.LookRotation( ( target.position + target.forward + Vector3.up * targetHeightRatio ) - transform.position, Vector3.up );
                }
            }
        }
    }

    public void ResetFov( float duration ) {
        if( duration == 0 ) {
            Camera.main.fieldOfView = defaultFOV;
        } else {
            StartCoroutine( ChangeFovCoroutine( defaultFOV, duration ) );
        }
    }

    public void ChangeFov( float to, float duration ) {
        if( duration == 0 ) {
            Camera.main.fieldOfView = defaultFOV;
        } else {
            StartCoroutine( ChangeFovCoroutine( to, duration ) );
        }
    }

    IEnumerator ChangeFovCoroutine( float to, float duration ) {
        float time = 0;
        float refVel = 0;
        while( time <= duration ) {
            Camera.main.fieldOfView = Mathf.SmoothDamp( Camera.main.fieldOfView, to, ref refVel, 0.15f, Mathf.Infinity, Time.fixedDeltaTime );
            time += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        Camera.main.fieldOfView = to;
    }

    public void Shake() {
        shake_intensity = 0.3f;
        shake_decay = 0.02f;
    }

    public void ResetToDefaults() {
        distance = defaultDistance;
        height = defaultHeight;
        topCameraOffset = defaultTopCameraOffset;
        targetHeightRatio = defaultTargetHeightRatio;
        heightDamping = defaultHeightDamping;
        rotationDamping = defaultRotationDamping;
        minHeight = defaultMinHeight;
    }

    void MagicCamera() {
        if( frontCarCamera ) {
            transform.position = vehicleFront.position;
            transform.rotation = vehicleFront.rotation;
        } else {
            float currentRotationAngle = transform.eulerAngles.y;
            float wantedHeight = target.position.y + height;

            if( autoRotate ) {
                wantedRotationAngle = target.eulerAngles.y;
            } else {
                //wantedRotationAngle = TouchSteering.instance.cameraRotation;
            }

            currentHeight = transform.position.y;

            // Damp the rotation around the y-axis
            currentRotationAngle = Mathf.LerpAngle( currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime );

            // Damp the height
            currentHeight = Mathf.Lerp( currentHeight, wantedHeight, heightDamping * Time.deltaTime );

            // Convert the angle into a rotation
            currentRotation = Quaternion.Euler( 0, currentRotationAngle, 0 );

            if( topCarCamera ) {
                transform.position = target.position + Vector3.up * topCameraOffset;
                transform.rotation = Quaternion.Euler( 90, currentRotationAngle, 0 );
            } else {
                transform.position = target.position;
                transform.position -= currentRotation * Vector3.forward * distance;
                tmp_vec.x = transform.position.x;
                tmp_vec.y = currentHeight;
                tmp_vec.z = transform.position.z;

                transform.position = tmp_vec;
            }

            if( !topCarCamera )
                transform.LookAt( target.position + Vector3.up * targetHeightRatio );
        }
    }
}