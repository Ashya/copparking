﻿using UnityEngine;
using System.Collections;

public class ParkingController : SingletonBehaviour<ParkingController> {

    [SerializeField]
    CarController carController;
    [SerializeField]
    CarControlls carControlls;
    Transform parkingPlaneTransform;

    [Separator("General settings")]
    [Range(0,10)]
    [SerializeField]
    private float speedOffset = 1f;

    [Separator("Distance to ParkingArea center")]
    public float distance = 0.0f;

    [Separator("Car wheels")]
    public Transform wheelFL;
    public Transform wheelFR;
    public Transform wheelRL;
    public Transform wheelRR;

    [Separator("General booleans")]
    public bool wheelFL_parked = false;
    public bool wheelFR_parked = false;
    public bool wheelRL_parked = false;
    public bool wheelRR_parked = false;

    private bool ready = false;
    public bool parked = false;
    public bool wheelsOnPlace = false;
    private RaycastHit hit;

    private bool created = false;
    private bool popupShown = false;

    private ParkingAreaController parkingAreaController;

    void Awake () {
        if( !created ) {
            created = true;
            GameManager.OnGameStart += this.OnGameStart;
            GameManager.OnGameEnd += this.OnGameEnd;
        }else {
            DestroyImmediate( gameObject );
        }
    }

    void OnGameStart() {
        carController = VehicleManager.instance.currentVehicle.GetComponent<CarController>();
        carControlls = VehicleManager.instance.currentVehicle.GetComponent<CarControlls>();

        parkingPlaneTransform = GameObject.Find( "ParkingPlane" ).transform;
        parkingAreaController = GameObject.Find( "ParkingArea" ).GetComponent<ParkingAreaController>();

        if(carController != null && 
            parkingPlaneTransform != null &&
            wheelFL != null &&
            wheelFR != null &&
            wheelRL != null &&
            wheelRR != null
            ) {
            ready = true;
        }
    }

    void OnGameEnd(bool win) {

    }

    void CheckWheels() {
        Ray ray1 = new Ray(wheelFL.position,Vector3.down);
        Ray ray2 = new Ray( wheelFR.position, Vector3.down );
        Ray ray3 = new Ray( wheelRL.position, Vector3.down );
        Ray ray4 = new Ray( wheelRR.position, Vector3.down );

        if(Physics.Raycast(ray1,out hit) && hit.collider.name == "ParkingPlane")
            wheelFL_parked = true;
        else
            wheelFL_parked = false;

        if( Physics.Raycast( ray2, out hit ) && hit.collider.name == "ParkingPlane" )
            wheelFR_parked = true;
        else
            wheelFR_parked = false;

        if( Physics.Raycast( ray3, out hit ) && hit.collider.name == "ParkingPlane" )
            wheelRL_parked = true;
        else
            wheelRL_parked = false;

        if( Physics.Raycast( ray4, out hit ) && hit.collider.name == "ParkingPlane" )
            wheelRR_parked = true;
        else
            wheelRR_parked = false;
    }

    void CheckIfCarParked() {
        if( WheelsOnPlace() && carController.CurrentSpeed <= speedOffset ) {
            parked = true;
        } else {
            parked = false;
        }
    }

    bool WheelsOnPlace() {
        if( wheelFL_parked && wheelFR_parked && wheelRL_parked && wheelRR_parked )
            wheelsOnPlace = true;
        else
            wheelsOnPlace = false;

        return wheelsOnPlace;
    }

    void EndGame() {
        GameManager.instance.GameOver(true);
    }

    void CalculateDistance() {
        float valX = this.transform.position.x - parkingPlaneTransform.position.x;
        float valY = this.transform.position.z - parkingPlaneTransform.position.z;

        distance = Mathf.Sqrt( Mathf.Pow( valX, 2 ) + Mathf.Pow( valY, 2 ) );
    }

	void Update () {
        if( !ready )
            return;

        CalculateDistance();

        CheckWheels();
        CheckIfCarParked();

        if( WheelsOnPlace() && 
            !GameManager.instance.gamePaused &&
            CheckpointController.instance.visitedCheckpoints != CheckpointController.instance.checkpoints.Count ) {
            if( !popupShown ) {
                popupShown = true;
                GUIController.instance.ShowNotParkedInfo();
            }
        } else {
            if( popupShown ) {
                popupShown = false;
                GUIController.instance.HideNotParkedInfo();
            }
        }

        if( parked && parkingAreaController.canBeParked ) {
            if( CheckpointController.instance.visitedCheckpoints == CheckpointController.instance.checkpoints.Count ) {
                carController.Move( 0, 0, 0, 1 );
                carControlls.enabled = false;
                EndGame();
            }
        }
    }

    /*void OnDrawGizmos() {
        Gizmos.color = Color.yellow;

        Gizmos.DrawLine( new Vector3( this.transform.position.x, this.transform.position.y, this.transform.position.z ), new Vector3( this.transform.position.x, this.transform.position.y + 10f, this.transform.position.z ) );
    }*/
}