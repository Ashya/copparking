﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckpointController : SingletonBehaviour<CheckpointController> {

    [HideInInspector]
    public GameObject gameLevels;

    [Separator("Main settings")]
    public Transform carTransform;
    public Transform checkpointsContainer;

    [Separator("Checkpoints list")]
    public List<GameObject> checkpoints;

    private int currentCheckpoint = 0;
    private int lastCheckpoint = -1;

    [Separator("Checkpoint settings")]
    [Range(1,100)]
    public float checkpointRadius = 10f;
    public int visitedCheckpoints = 0;

    private bool ready = false;
    private bool created = false;

    void Awake() {
        if(!created) {
            checkpoints = new List<GameObject>();
            GameManager.OnGameStart += this.OnGameStart;
        }else {
            DestroyImmediate( instance );
        }
    }

    public void Reset() {
        ready = false;

        currentCheckpoint = 0;
        lastCheckpoint = currentCheckpoint - 1;
        checkpoints.Clear();
    }

    void OnGameStart() {
        visitedCheckpoints = 0;
        checkpointsContainer = GameObject.Find( "Checkpoints" ).transform;
        carTransform = VehicleManager.instance.currentVehicle.GetComponent<Transform>();

        foreach( Transform checkpoint in checkpointsContainer) {
            checkpoints.Add( checkpoint.gameObject );
            checkpoint.gameObject.SetActive( false );
        }
        checkpoints.Sort( ( x, y ) => string.Compare( x.name, y.name ) );

        checkpoints[currentCheckpoint].SetActive( true );

        ready = true;
    }

    bool Close(Vector3 v1, Vector3 v2) {
        Debug.DrawLine( v1, v2, Color.blue );
        return Vector3.SqrMagnitude( v1 - v2 ) < checkpointRadius;
    }

    void VisitCheckpoint() {
        if( currentCheckpoint < checkpoints.Count ) {
            if( Close( checkpoints[currentCheckpoint].transform.position, carTransform.position ) ) {
                checkpoints[currentCheckpoint].SetActive( false );

                lastCheckpoint = currentCheckpoint;
                currentCheckpoint++;

                visitedCheckpoints++;

                if( currentCheckpoint < checkpoints.Count ) {
                    checkpoints[currentCheckpoint].SetActive( true );
                }
            }
        }
    }

    void FixedUpdate() {
        if( !ready )
            return;

        if( GameManager.instance.inGame ) {
            VisitCheckpoint();

            if( visitedCheckpoints == checkpoints.Count )
                ParkingAreaController.instance.canBeParked = true;
        }
    }
}
